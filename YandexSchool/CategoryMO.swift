//
//  Category.swift
//  YandexSchool
//
//  Created by Alex on 03.08.15.
//  Copyright (c) 2015 Alexey Sidorov. All rights reserved.
//

import Foundation
import CoreData

class CategoryMO: NSManagedObject {

    @NSManaged var key: NSNumber
    @NSManaged var title: String
    @NSManaged var relationship: NSOrderedSet

    func asDict() -> Dictionary<String, AnyObject> {
        return ["key" : key, "title" : title, "subs" : subsAsDict()]
    }
    
    func subsAsDict() -> [Dictionary<String, AnyObject>] {
        var dictArr = [Dictionary<String, AnyObject>]()
        for sub in relationship {
            dictArr.append(sub.asDict())
        }
        return dictArr
    }
    
}
