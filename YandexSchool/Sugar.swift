//
//  Sugar.swift
//  YandexSchool
//
//  Created by Alexey Sidorov on 04/08/15.
//  Copyright (c) 2015 Alexey Sidorov. All rights reserved.
//

import Foundation

extension Array {
    subscript (safe index: Int) -> T? {
        return index >= 0 && index < self.count
            ? self[index]
            : nil
    }
}
