//
//  CategoryFacade.swift
//  YandexSchool
//
//  Created by Alex on 03.08.15.
//  Copyright (c) 2015 Alexey Sidorov. All rights reserved.
//

import Foundation
import CoreData
import ObjectMapper

class CategoryFacade {
    
    internal class func categories(forceRefresh: Bool = false, completion: (categories: [Category]?) -> Void)  {
        
        let categories = CategoryCoreDataAdapter.loadCategories()
        
        if forceRefresh || (categories?.isEmpty ?? false) {
            APIWrapper.loadCategories( { (JSON, error) -> () in
                if let categories = Mapper<Category>().mapArray(JSON) {
                    completion(categories: categories)
                    
                    //save to CoreData
                    if let managedContext = AppDelegate.defaultManagedObjectContext {
                        for category in categories {
                            var categ = category
                            if let categoryMO = CategoryCoreDataAdapter.insertCategory() {
                                categoryMO.title = category.title ?? ""
                                
                                if categ.subcategories?.count > 0 {
                                    CategoryFacade.setSubcategories(categoryMO, subs: categ.subcategories!)
                                }
                            }
                        }
                    }
                }
            })
        }
        else {
            var categoriesModels: [Category] = []
            for category in categories ?? [] {
                if let categoryModel = Mapper<Category>().map(category.asDict()) {
                    categoriesModels.append(categoryModel)
                }
            }
            
            if !categoriesModels.isEmpty {
                completion(categories: categoriesModels.filter({$0.key == nil}))
            }
        }
    }
    
    static func setSubcategories(obj: CategoryMO, subs: [Category]) {
        var subcats = NSMutableOrderedSet()
        for subcat in subs {
            if let subcategory = CategoryCoreDataAdapter.insertCategory() {
                subcategory.title = subcat.title ?? ""
                subcategory.key = subcat.key ?? 0
                if nil != subcat.subcategories && !subcat.subcategories!.isEmpty {
                    setSubcategories(subcategory, subs: subcat.subcategories!)
                }
                subcats.addObject(subcategory)
            }
        }
        obj.relationship = subcats.copy() as! NSOrderedSet
    }
}

class CoreDataAdapter {
    
    private class func loadEntities(entityName: String) -> [AnyObject]? {
        let fetchRequest = NSFetchRequest(entityName: entityName)
        fetchRequest.includesSubentities = true
        var error: NSError?
        let data = AppDelegate.defaultManagedObjectContext?.executeFetchRequest(fetchRequest, error: &error) as [AnyObject]?
        return data
    }
    
    private class func insertEntity(entityName: String) -> AnyObject? {
        if let context = AppDelegate.defaultManagedObjectContext {
            return NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: context)
        }
        return nil
    }
}

class CategoryCoreDataAdapter: CoreDataAdapter {
    
    private static let modelName: String = "CategoryMO"
    
    class func loadCategories() -> [CategoryMO]? {
        return self.loadEntities(CategoryCoreDataAdapter.modelName) as! [CategoryMO]?
    }
    
    class func insertCategory() -> CategoryMO? {
        return self.insertEntity(CategoryCoreDataAdapter.modelName) as! CategoryMO?
    }
}










