//
//  APIWrapper.swift
//  YandexSchool
//
//  Created by Alex on 03.08.15.
//  Copyright (c) 2015 Alexey Sidorov. All rights reserved.
//

import Foundation
import Alamofire



//https://money.yandex.ru/api/categories-list

class APIWrapper {
    private static let apiURL = NSURL(string: "https://money.yandex.ru/api/categories-list")
    
    required init() {
        
    }
    
    internal typealias CategoriesCompletionBlock = (JSON: AnyObject?, error: NSError?) -> ()
    
    static func loadCategories(completion: CategoriesCompletionBlock) {
        Alamofire.request(.GET, apiURL!, parameters: nil)
            .responseJSON { (request, response, JSON, error) in
                completion(JSON: JSON, error: error)
        }
    }
    
}