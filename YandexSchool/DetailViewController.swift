//
//  DetailViewController.swift
//  YandexSchool
//
//  Created by Alexey Sidorov on 04/08/15.
//  Copyright (c) 2015 Alexey Sidorov. All rights reserved.
//

import UIKit
import Foundation

class DetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var subcategories: [Category] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }
    
    // MARK: - UITableView delegate + data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subcategories.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! UITableViewCell
        
        if let subcategory = subcategories[safe: indexPath.row] {
            cell.textLabel?.text = subcategory.title
        }
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "show-detail" {
            if let cell = sender as? UITableViewCell {
                if let indexPath = tableView.indexPathForCell(cell) {
                    let dvc: DetailViewController = segue.destinationViewController as! DetailViewController
                    dvc.subcategories = subcategories[safe: indexPath.row]?.subcategories ?? []
                }
            }
        }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        if identifier == "show-detail" {
            if let cell = sender as? UITableViewCell {
                if let indexPath = tableView.indexPathForCell(cell) {
                    if let subcategory = subcategories[safe: indexPath.row] {
                        return nil != subcategory.subcategories && !subcategory.subcategories!.isEmpty
                    }
                }
            }
        }
        return true
    }
}
