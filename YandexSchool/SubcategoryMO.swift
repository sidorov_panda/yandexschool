//
//  Subcategory.swift
//  YandexSchool
//
//  Created by Alex on 03.08.15.
//  Copyright (c) 2015 Alexey Sidorov. All rights reserved.
//

import Foundation
import CoreData

class SubcategoryMO: NSManagedObject {

    @NSManaged var key: NSNumber
    @NSManaged var title: String
    @NSManaged var relationship: NSOrderedSet

    func asDict() -> Dictionary<String, AnyObject> {
        var dict = ["id" : key, "title" : title]
        if relationship.count > 0 {
            dict["subs"] = self.relationshipAsDict()
        }
        return dict
    }
    
    func relationshipAsDict() -> NSOrderedSet {
        var ret = NSMutableOrderedSet()
        for obj in relationship ?? [] {
            if let sub = obj as? SubcategoryMO {
                ret.addObject(sub.asDict())
            }
        }
        return ret.copy() as! NSOrderedSet
    }
    
}
