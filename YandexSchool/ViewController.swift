//
//  ViewController.swift
//  YandexSchool
//
//  Created by Alexey Sidorov on 03/08/15.
//  Copyright (c) 2015 Alexey Sidorov. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData(force: false)
    }
    
    private var data: [Category] = []

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func loadData(force: Bool = false) {
        CategoryFacade.categories(forceRefresh: force,
            completion: { [weak self] (categories) -> Void in
            if let categs = categories {
                self?.data = categs
                self?.tableView.reloadData()
            }
            else {
                //show error here
            }
        })
    }
    
    // MARK: - Actions
    
    @IBAction func refreshBtnDidTap(sender: AnyObject) {
        loadData(force: true)
    }

    // MARK: - Table View
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! UITableViewCell
        
        if let category = data[safe: indexPath.row] {
            cell.textLabel?.text = category.title
        }
        
        return cell
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "show-detail" {
            
            if let cell = sender as? UITableViewCell {
                if let indexPath = tableView.indexPathForCell(cell) {
                    let dvc: DetailViewController = segue.destinationViewController as! DetailViewController
                    dvc.subcategories = data[safe: indexPath.row]?.subcategories ?? []
                }
            }
        }
    }
    
}

