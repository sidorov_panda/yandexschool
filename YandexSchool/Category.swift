//
//  Category.swift
//  YandexSchool
//
//  Created by Alex on 03.08.15.
//  Copyright (c) 2015 Alexey Sidorov. All rights reserved.
//

import Foundation
import ObjectMapper

class Category: Mappable {
    
    // MARK: - init
    
    static func newInstance() -> Mappable {
        return Category()
    }
    
    // MARK: - Props
    var key: Int?
    var title: String?
    var subcategories: [Category]? = []
    
    // MARK: - Mappable
    
    func mapping(map: Map) {
        key             <- map["id"]
        title           <- map["title"]
        subcategories   <- map["subs"]
    }
    
}

class Subcategory: Mappable {
    
    // MARK: - init
    
    static func newInstance() -> Mappable {
        return Subcategory()
    }
    
    // MARK: - Props
    
    var key: Int?
    var title: String?
    var subcategories: [Subcategory]? = []
    
    // MARK: - Mappable
    
    func mapping(map: Map) {
        key             <- map["id"]
        title           <- map["title"]
        subcategories   <- map["subs"]
    }
}